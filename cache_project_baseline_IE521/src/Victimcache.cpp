/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
       	                  	 entry* vc_cache_blocks,
        	                 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	         bool debug)
{
	int aux_position;  
   	bool hit_flag = false;
   	bool not_full_block = false;
   	for(int i = 0; i < l1_vc_info->l1_assoc; i++){
    	if(l1_vc_info->l1_tag == l1_cache_blocks[i].tag){
        	if(l1_cache_blocks[i].valid == true){
            	aux_position = i;
            	if(loadstore == false){
            	   l1_cache_blocks[i].dirty = false;
            	   l1_result->miss_hit = HIT_LOAD;
            	}
            	else{
               		l1_cache_blocks[i].dirty = true;
               		l1_result->miss_hit = HIT_STORE;
            	}
            	l1_result->dirty_eviction = false;
				for(int i = 0; i < l1_vc_info->l1_assoc; i++){
      				if(i == aux_position){
         				l1_cache_blocks[i].rp_value = 0;
      				}
      				if(l1_cache_blocks[i].tag != -1){
         				l1_cache_blocks[i].rp_value += 1;
      				}
      				else if(l1_cache_blocks[i].tag == -1){
         				break;
      				}
   				}
            	hit_flag = true;
            	break;
         	}
      	}
        if(l1_cache_blocks[i].tag == -1){
        	not_full_block = true;
      	}
   	}
   	if(hit_flag == false){
    	if(loadstore == false){
        	l1_result->miss_hit = MISS_LOAD;
        }
      	else{
         	l1_result->miss_hit = MISS_STORE;
      	}
      	if(not_full_block == true){
			for(int i = 0; i < l1_vc_info->l1_assoc; i++){
      			if((i == 0) & (l1_cache_blocks[i].tag == -1)){
         			l1_cache_blocks[i].tag = l1_vc_info->l1_tag;
         			l1_cache_blocks[i].rp_value = 0;
         			l1_cache_blocks[i].valid = true;
         			if(loadstore == false){
            			l1_cache_blocks[i].dirty = false;
         			}
         			else{
            			l1_cache_blocks[i].dirty = true;
         			}
         			break;
      			}
      			else if(l1_cache_blocks[i].tag == -1){
         			l1_cache_blocks[i].tag = l1_vc_info->l1_tag;
         			l1_cache_blocks[i].rp_value = 0;
         			l1_cache_blocks[i].valid = true;
         			if(loadstore == false){
            			l1_cache_blocks[i].dirty = false;
         			}
         			else{
            			l1_cache_blocks[i].dirty = true;
         			}
         			int aux_position = i;
         			bool finish = false;
         			while(finish == false){
            			aux_position -= 1;
            			l1_cache_blocks[aux_position].rp_value += 1;
            			if(aux_position == 0){
               				finish = true;
            			}
         			}
         			break;
      			}
   			}
      	}
      	else{
			int position = l1_cache_blocks[0].rp_value;
   			for(int i = 0; i < l1_vc_info->l1_assoc; i++){
      			if(l1_cache_blocks[i].tag != -1){
         			if(position < l1_cache_blocks[i].rp_value){
            			position = l1_cache_blocks[i].rp_value;
         			}
      			}
      			else{
         			break;
      			}
   			}
   			if(l1_cache_blocks[position].dirty == false){
      			l1_result->dirty_eviction = false;
   			}
   			else{
      			l1_result->dirty_eviction = true;
      			l1_result->evicted_address = l1_cache_blocks[position].tag;
   			}
   			l1_cache_blocks[position].tag = l1_vc_info->l1_tag;
   			l1_cache_blocks[position].valid = true;
   			if(loadstore == false){
   			   l1_cache_blocks[position].dirty = false;
   			}
   			else{
   			   l1_cache_blocks[position].dirty = true;
   			}
			for(int i = 0; i < l1_vc_info->l1_assoc; i++){
      			if(i == position){
      			   l1_cache_blocks[i].rp_value = 0;
      			}
      			if(l1_cache_blocks[i].tag != -1){
      			   l1_cache_blocks[i].rp_value += 1;
      			}
      			else if(l1_cache_blocks[i].tag == -1){
      			   break;
      			}
   			}
			for(int i = 0; i < l1_vc_info->l1_assoc; i++){
      			if(i == position){
      			   l1_cache_blocks[i].rp_value = 0;
      			}
      			if(l1_cache_blocks[i].tag != -1){
      			   l1_cache_blocks[i].rp_value += 1;
      			}
      			else if(l1_cache_blocks[i].tag == -1){
      			   break;
      			}
   			}
      	}
   	}
	//Aquí empieza el victim cache
	bool hit_flag_vc = false;
	int largest_pos = vc_cache_blocks[0].rp_value;
	if (l1_result->miss_hit == MISS_LOAD || l1_result->miss_hit == MISS_STORE){
		if(l1_result->dirty_eviction == true){
			for(int i = 0; i < l1_vc_info->vc_assoc; i++){
				if(vc_cache_blocks[i].valid == true){
					if(vc_cache_blocks[i].tag == l1_result->evicted_address){
						vc_result->dirty_eviction = l1_result->dirty_eviction;
						vc_result->evicted_address = vc_cache_blocks[i].tag;
						hit_flag_vc = true;
					}
				}	
			}
			if(hit_flag_vc == false){
				for(int j = 0; j < l1_vc_info->vc_assoc; j++){
					if(vc_cache_blocks[j].valid == true){
						if(largest_pos < vc_cache_blocks[j].rp_value){
							largest_pos = vc_cache_blocks[j].rp_value;
						}
					}
				}
				vc_cache_blocks[largest_pos].tag = l1_result->evicted_address;
				vc_cache_blocks[largest_pos].valid = true;
				if(l1_result->dirty_eviction == false){
					vc_cache_blocks[largest_pos].dirty = false;
				}
				else{
					vc_cache_blocks[largest_pos].dirty = true;
				}
				for(int j = 0; j < l1_vc_info->vc_assoc; j ++){
					if(j != largest_pos){
						vc_cache_blocks[largest_pos].rp_value += 1;
					}
				}
			}
		}
	}
   	return OK;
}