#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>

using namespace std;
#define miss_penalty 20

/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  printf("Do something :), don't forget to keep track of execution time");
  printf("\n");
  int a = stoi(argv[2]);  //Associativity
  int s = stoi(argv[4]);  //Cache size KB
  int l = stoi(argv[6]);  //Block size in bytes
  int rp = stoi(argv[8]); //Replacement policy
  struct cache_params params = {s,a,l};
  struct cache_field_size field_size;
  int set_num = field_size_get(params,&field_size);
  long address;
  int idx;
  int tag;
  bool loadstore;
  int IC;
  int val;
  int total_cycles = 0;
  int evictions = 0;
  struct entry **cache = set_cache(set_num,a);
  bool debug = false;
  struct operation_result result;
  struct counter_hit_mis counter_hit_mis;
  int hit_miss;
  int counter = 0;
  string str;
  while (cin >> str){
    if(str != "#"){
      switch (counter){
        case 0:
          if(str == "1"){
            loadstore = true;
          }
          else{
            loadstore = false;
          }
          counter ++;
          break;
        case 1: 
          address = stol(str);
          counter ++;
          break;
        case 2:
          IC = stoi(str);
          counter = 0;
          address_tag_idx_get(address,field_size,&idx,&tag);
          switch (rp){
            case LRU:
              lru_replacement_policy(idx,tag,a,loadstore,&(*(cache[idx])),&result);
              hit_miss_counter(&counter_hit_mis,&result);
              if(result.dirty_eviction == true){
                evictions ++;
              }
              if((result.miss_hit == MISS_LOAD) || (result.miss_hit == MISS_STORE)){
                if(loadstore == false){
                  total_cycles = total_cycles + IC*(1+miss_penalty);
                }
                else{
                  total_cycles = total_cycles + IC;
                }
              }
              else{
                total_cycles = total_cycles + IC;
              }
              break;
            case NRU:
              nru_replacement_policy(idx,tag,a,loadstore,&(*(cache[idx])),&result);
              hit_miss_counter(&counter_hit_mis,&result);
              if(result.dirty_eviction == true){
                evictions ++;
              }
              if((result.miss_hit == MISS_LOAD) || (result.miss_hit == MISS_STORE)){
                if(loadstore == false){
                  total_cycles = total_cycles + IC*(1+miss_penalty);
                }
                else{
                  total_cycles = total_cycles + IC;
                }
              }
              else{
                total_cycles = total_cycles + IC;
              }
              break;    
            case RRIP:
              srrip_replacement_policy(idx,tag,a,loadstore,&(*(cache[idx])),&result);
              hit_miss_counter(&counter_hit_mis,&result);
              if(result.dirty_eviction == true){
                evictions ++;
              }
              if((result.miss_hit == MISS_LOAD) || (result.miss_hit == MISS_STORE)){
                if(loadstore == false){
                  total_cycles = total_cycles + IC*(1+miss_penalty);
                }
                else{
                  total_cycles = total_cycles + IC;
                }
              }
              else{
                total_cycles = total_cycles + IC;
              }
              break;
            case RANDOM:
              val = rand() % 2;
              switch (val){
                case 0:
                lru_replacement_policy(idx,tag,a,loadstore,&(*(cache[idx])),&result);
                hit_miss_counter(&counter_hit_mis,&result);
                if(result.dirty_eviction == true){
                  evictions ++;
                }
                if((result.miss_hit == MISS_LOAD) || (result.miss_hit == MISS_STORE)){
                  if(loadstore == false){
                    total_cycles = total_cycles + IC*(1+miss_penalty);
                  }
                  else{
                    total_cycles = total_cycles + IC;
                  }
                }
                else{
                  total_cycles = total_cycles + IC;
                }
                break; 
                case 1:
                  nru_replacement_policy(idx,tag,a,loadstore,&(*(cache[idx])),&result);
                  hit_miss_counter(&counter_hit_mis,&result);
                  if(result.dirty_eviction == true){
                    evictions ++;
                  }
                  if((result.miss_hit == MISS_LOAD) || (result.miss_hit == MISS_STORE)){
                    if(loadstore == false){
                      total_cycles = total_cycles + IC*(1+miss_penalty);
                    }
                    else{
                      total_cycles = total_cycles + IC;
                    }
                  }
                  else{
                    total_cycles = total_cycles + IC;
                  }
                  break;  
                case 2:
                  srrip_replacement_policy(idx,tag,a,loadstore,&(*(cache[idx])),&result);
                  hit_miss_counter(&counter_hit_mis,&result);
                  if(result.dirty_eviction == true){
                    evictions ++;
                  }
                  if((result.miss_hit == MISS_LOAD) || (result.miss_hit == MISS_STORE)){
                    if(loadstore == false){
                      total_cycles = total_cycles + IC*(1+miss_penalty);
                    }
                    else{
                      total_cycles = total_cycles + IC;
                    }
                  }
                  else{
                    total_cycles = total_cycles + IC;
                  }
                    break;
                default:
                  break;  
              }
              break;
            default:
              printf("You didn't choose one of the available replacement policies\n");
              break;
          }
          break;
        default:
          break;
      }
    }
  }

   
  float overall_miss_rate = (counter_hit_mis.miss_store+counter_hit_mis.miss_load)/(counter_hit_mis.miss_store+counter_hit_mis.miss_load+counter_hit_mis.hit_load+counter_hit_mis.hit_store);
  float read_miss_rate = (counter_hit_mis.miss_load)/(counter_hit_mis.miss_store+counter_hit_mis.miss_load+counter_hit_mis.hit_load+counter_hit_mis.hit_store);
  cout << "----------------------------------------------------------" << endl;
  cout << "Cache parameters" << endl;
  cout << "----------------------------------------------------------" << endl;
  cout << "Cache Size (KB): " << s << "KB" << endl;
  cout << "Cache Associativity: " << a << endl;
  cout << "Cache Block Size (bytes): " << l << endl;
  cout << "----------------------------------------------------------" << endl;
  cout << "Simulation results:" << endl;
  cout << "----------------------------------------------------------" << endl;
  cout << "CPU time (cycles): " << total_cycles <<endl;
  cout << "AMAT(cycles): " << endl;
  cout << "Overall miss rate: " << overall_miss_rate << endl;
  cout << "Read miss rate: " << read_miss_rate <<endl;
  cout << "Dirty evictionss: " << evictions << endl;
  cout << "Load misses: " << counter_hit_mis.miss_load << endl;
  cout << "Store misses: " << counter_hit_mis.miss_store << endl;
  cout << "Total misses: " << (counter_hit_mis.miss_store+counter_hit_mis.miss_load) << endl;
  cout << "Load hits: " << counter_hit_mis.hit_load << endl;
  cout << "Store hits: " << counter_hit_mis.hit_store << endl;
  cout << "Total hits: " << (counter_hit_mis.hit_load+counter_hit_mis.hit_store) << endl;
  cout << "----------------------------------------------------------" << endl;


  return 0;
}
