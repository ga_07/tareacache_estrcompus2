/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>


#define KB 1024
#define ADDRSIZE 32
using namespace std;   

struct entry** set_cache(int sets,int ways)
{
   struct entry** cache = new struct entry*[sets];
   struct entry position;
   for(int i = 0; i < sets; i++){
      cache[i] = new struct entry[ways];
      for (int j = 0; j < ways; j++){
         cache[i][j] = position;
      }
   }
   return cache;
}

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
   int offset = log2(cache_params.block_size); 
   int set_num = (cache_params.size*KB)/(cache_params.block_size*cache_params.asociativity);
   int idx = log2(set_num);
   int tag = ADDRSIZE-idx-offset;
   field_size->tag = tag;
   field_size->idx = idx;
   field_size->offset = offset;
   return set_num;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
   int aux = address & (field_size.idx+field_size.offset);
   *(idx) = aux >> field_size.offset;
   *(tag) = address >> (field_size.idx+field_size.offset);
}

int srrip_replacement_policy(int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   int aux;
   bool hit_flag = false;
   int M;
   int aux_result;
   if (associativity <= 2){
      //Exactly the same as NRU policy
      M = 1;
      aux_result = nru_replacement_policy(idx, tag, associativity, loadstore, cache_blocks, result, debug);
      return aux_result;
   }
   else{
      //SRRIP cases
      M = 2;
   }
   int RRPV_new = 2 * pow(2, M) - 2; //2|6
   for (int i = 0; i < associativity; i++)
   {
      if (tag == cache_blocks[i].tag){
         if (cache_blocks[i].valid == true){
            if (loadstore == false){
               cache_blocks[i].dirty = false;
               result->miss_hit = HIT_LOAD;
            }
            else{
               cache_blocks[i].dirty = true;
               result->miss_hit = HIT_STORE;
            }
            result->dirty_eviction = false;
            result->evicted_address = 0;
            cache_blocks[i].rp_value = 0;
            hit_flag = true;
            break;
         }
      }
   }
   //MISS case
   if (hit_flag == false)
   {
      if (loadstore == false){
         result->miss_hit = MISS_LOAD;
      }
      else{
         result->miss_hit = MISS_STORE;
      }
      for (int j = 0; j < associativity; j++){
         if (cache_blocks[j].rp_value == 3){
            if (cache_blocks[j].dirty == false){
               result->dirty_eviction = false;
               result->evicted_address = 0;
            }
            else{
               result-> dirty_eviction = true;
               result-> evicted_address = cache_blocks[j].tag;
            }
            cache_blocks[j].tag = tag;
            cache_blocks[j].valid = true;
            cache_blocks[j].rp_value = RRPV_new;
            if (loadstore == false){
               cache_blocks[j].dirty = false;
            }
            else{
               cache_blocks[j].dirty = true;
            }
            break;
         }
         else if (j == associativity-1) {
            if (cache_blocks[j].rp_value != 3){
               for (int k = 0; k < associativity; k++){
                  cache_blocks[k].rp_value = cache_blocks[k].rp_value + 1;
               }
               srrip_replacement_policy(idx, tag, associativity, loadstore, cache_blocks, result, debug);
            }
            else{
               cache_blocks[j].tag = tag;
               cache_blocks[j].valid = true;
               cache_blocks[j].rp_value = RRPV_new;
            }
         }
      }
   }
   return aux_result;
}

int lru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry *cache_blocks,
                           operation_result *result,
                           bool debug)
{
   int aux_position;
   bool hit_flag = false;
   bool not_full_block = false;
   for (int i = 0; i < associativity; i++)
   {
      if (tag == cache_blocks[i].tag)
      {
         if (cache_blocks[i].valid == true)
         {
            aux_position = i;
            if (loadstore == false)
            {
               cache_blocks[i].dirty = false;
               result->miss_hit = HIT_LOAD;
            }
            else
            {
               cache_blocks[i].dirty = true;
               result->miss_hit = HIT_STORE;
            }
            result->dirty_eviction = false;
            update_LRU(cache_blocks, aux_position, associativity);
            hit_flag = true;
            break;
         }
      }
      if (cache_blocks[i].tag == -1)
      {
         not_full_block = true;
      }
   }
   if (hit_flag == false)
   {
      if (loadstore == false)
      {
         result->miss_hit = MISS_LOAD;
      }
      else
      {
         result->miss_hit = MISS_STORE;
      }
      if (not_full_block == true)
      {
         not_full(cache_blocks, associativity, tag, loadstore);
      }
      else
      {
         miss_LRU(cache_blocks, result, associativity, tag, loadstore);
      }
   }
   return OK;
}

void update_LRU(entry *cache_blocks,
                int position,
                int associativity)
{
   for (int i = 0; i < associativity; i++)
   {
      if (i == position)
      {
         cache_blocks[i].rp_value = 0;
      }
      if (cache_blocks[i].tag != -1)
      {
         cache_blocks[i].rp_value += 1;
      }
      else if (cache_blocks[i].tag == -1)
      {
         break;
      }
   }
}

void not_full(entry *cache_blocks,
              int associativity,
              int tag,
              bool loadstore)
{
   for (int i = 0; i < associativity; i++)
   {
      if ((i == 0) & (cache_blocks[i].tag == -1))
      {
         cache_blocks[i].tag = tag;
         cache_blocks[i].rp_value = 0;
         cache_blocks[i].valid = true;
         if (loadstore == false)
         {
            cache_blocks[i].dirty = false;
         }
         else
         {
            cache_blocks[i].dirty = true;
         }
         break;
      }
      else if (cache_blocks[i].tag == -1)
      {
         cache_blocks[i].tag = tag;
         cache_blocks[i].rp_value = 0;
         cache_blocks[i].valid = true;
         if (loadstore == false)
         {
            cache_blocks[i].dirty = false;
         }
         else
         {
            cache_blocks[i].dirty = true;
         }
         int aux_position = i;
         bool finish = false;
         while (finish == false)
         {
            aux_position -= 1;
            cache_blocks[aux_position].rp_value += 1;
            if (aux_position == 0)
            {
               finish = true;
            }
         }
         break;
      }
   }
}

void miss_LRU(entry *cache_blocks,
              operation_result *result,
              int associativity,
              int tag,
              bool loadstore)
{
   int position = cache_blocks[0].rp_value;
   for (int i = 0; i < associativity; i++)
   {
      if (cache_blocks[i].tag != -1)
      {
         if (position < cache_blocks[i].rp_value)
         {
            position = cache_blocks[i].rp_value;
         }
      }
      else
      {
         break;
      }
   }
   if (cache_blocks[position].dirty == false)
   {
      result->dirty_eviction = false;
   }
   else
   {
      result->dirty_eviction = true;
      result->evicted_address = cache_blocks[position].tag;
   }
   cache_blocks[position].tag = tag;
   cache_blocks[position].valid = true;
   if (loadstore == false)
   {
      cache_blocks[position].dirty = false;
   }
   else
   {
      cache_blocks[position].dirty = true;
   }
   update_LRU(cache_blocks, position, associativity);
}

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry *cache_blocks,
                           operation_result *operation_result,
                           bool debug)
{
   bool hit_flag = false;
   for (int i = 0; i < associativity; i++)
   {
      if (tag == cache_blocks[i].tag)
      {
         if (cache_blocks[i].valid == true)
         {
            if (loadstore == false)
            {
               cache_blocks[i].dirty = false;
               operation_result->miss_hit = HIT_LOAD;
            }
            else
            {
               cache_blocks[i].dirty = true;
               operation_result->miss_hit = HIT_STORE;
            }
            operation_result->dirty_eviction = false;
            cache_blocks[i].tag = tag;
            cache_blocks[i].rp_value = 0;
            hit_flag = true;
            break;
         }
      }
   }
   if (hit_flag == false)
   {
      if (loadstore == false)
      {
         operation_result->miss_hit = MISS_LOAD;
      }
      else
      {
         operation_result->miss_hit = MISS_STORE;
      }
      int miss_pos;
      bool founded = false;
      nru_find_1(associativity, cache_blocks, &miss_pos, &founded);
      if (founded == false)
      {
         for (int j = 0; j < associativity; j++)
         {
            cache_blocks[j].rp_value = 1;
         }
         nru_find_1(associativity, cache_blocks, &miss_pos, &founded);
         if (cache_blocks[miss_pos].dirty == false)
         {
            operation_result->dirty_eviction = false;
         }
         else
         {
            operation_result->dirty_eviction = true;
            operation_result->evicted_address = cache_blocks[miss_pos].tag;
         }
         cache_blocks[miss_pos].tag = tag;
         cache_blocks[miss_pos].valid = true;
         cache_blocks[miss_pos].rp_value = 0;
         if (loadstore == false)
         {
            cache_blocks[miss_pos].dirty = false;
         }
         else
         {
            cache_blocks[miss_pos].dirty = true;
         }
      }
      else
      {
         if (cache_blocks[miss_pos].dirty == false)
         {
            operation_result->dirty_eviction = false;
         }
         else
         {
            operation_result->dirty_eviction = true;
            operation_result->evicted_address = cache_blocks[miss_pos].tag;
         }
         cache_blocks[miss_pos].tag = tag;
         cache_blocks[miss_pos].valid = true;
         cache_blocks[miss_pos].rp_value = 0;
         if (loadstore == false)
         {
            cache_blocks[miss_pos].dirty = false;
         }
         else
         {
            cache_blocks[miss_pos].dirty = true;
         }
      }
   }
   return OK;
}

void nru_find_1(int associativity,
                entry *cache_blocks,
                int *miss_pos,
                bool *founded)
{
   for (int j = 0; j < associativity; j++)
   {
      if (cache_blocks[j].rp_value == 1)
      {
         *(miss_pos) = j;
         *(founded) = true;
      }
   }
}

void hit_miss_counter(counter_hit_mis *counter_hit_mis,
                      operation_result *result)
{
   switch (result->miss_hit)
   {
   case MISS_LOAD:
      counter_hit_mis->miss_load += 1;
      break;
   case MISS_STORE:
      counter_hit_mis->miss_store += 1;
      break;
   case HIT_LOAD:
      counter_hit_mis->hit_load += 1;
      break;
   case HIT_STORE:
      counter_hit_mis->hit_store += 1;
      break;
   default:
      break;
   }
}
