/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
				entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				operation_result* operation_result_cache_obl,
                                bool debug=false)
{
        bool hit_flag = false;

        cache_block[associativity] = {};
        cache_block_obl[associativity] = {};

        for (int i = 0; i<associativity; i++){
                //hit
                if (tag == cache_block[i].tag){
                        hit_flag = true;
                        if (cache_block_obl[i].obl_tag != 0){
                                cache_block_obl[i+1].obl_tag = 0;
                                cache_block[i].obl_tag = 0;
                                if (loadstore=false){
                                        operation_result_cache_block->miss_hit = HIT_LOAD;
                                }
                                else {
                                        operation_result_cache_block->miss_hit = HIT_STORE;
                                }
                        }
                        else{
                                cache_block_obl[i].obl_tag = 1;
                                cache_block[i].obl_tag = 1;
                        }
                }
                //miss
                if(i == associativity-1 && hit_flag == false && tag != cache_block[i].tag){
                        cache_block[i].obl_tag = 0;
                        cache_block_obl[i+1].obl_tag = 1;
                        if (loadstore = false){
                                operation_result_cache_block->miss_hit = MISS_LOAD;
                        }
                        else{
                                operation_result_cache_block->miss_hit = MISS_STORE;
                        }
                        lru_replacement_policy(idx, cache_block[i].tag, associativity, loadstore, &cache_block[i], &operation_result_cache_block[i], debug);
                }
        }
}


