/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_l2(const l1_l2_entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{
	int aux_position;
	bool hit_flag_l1 = false;
	bool hit_flag_l2 = false;
	bool not_full_block = false;
	//checks for hit in l1
	for (int i = 0; i < l1_l2_info->l1_associativity ; i++)
	{
		if (l1_l2_info->l1_tag == l1_cache_blocks[i].tag)
		{
			if (l1_cache_blocks[i].valid == true)
			{
				aux_position = i;
				if (loadstore == false)
				{
					l1_cache_blocks[i].dirty = false;
					l1_result->miss_hit = HIT_LOAD;
				}
				else
				{
					l1_cache_blocks[i].dirty = true;
					l1_result->miss_hit = HIT_STORE;
				}
				l1_result->dirty_eviction = false;
				update_LRU(l1_cache_blocks, aux_position, l1_l2_info->l1_associativity);
				hit_flag_l1 = true;
				break;
			}
		}
		if (l1_cache_blocks[i].tag == -1)
		{
			not_full_block = true;
		}
	}
	//checks for hit in l2
	if (hit_flag_l1 == false)
	{
		for (int j = 0; j < l1_l2_info->l2_associativity; j++)
		{
			if (l1_l2_info->l2_tag == l2_cache_blocks[j].tag)
			{
				if (l2_cache_blocks[j].valid == true)
				{
					aux_position = j;
					if (loadstore == false)
					{
						l2_cache_blocks[j].dirty = false;
						l2_result->miss_hit = HIT_LOAD;
					}
					else
					{
						l2_cache_blocks[j].dirty = true;
						l2_result->miss_hit = HIT_STORE;
					}
					l2_result->dirty_eviction = false;
					update_LRU(l2_cache_blocks, aux_position, l1_l2_info->l2_associativity);
					hit_flag_l2 = true;
					break;
				}
			}
			if (l2_cache_blocks[j].tag == -1)
			{
				not_full_block = true;
			}
		}
	}
	if (hit_flag_l2==false){
		if (loadstore == false)
		{
			l2_result->miss_hit = MISS_LOAD;
		}
		else
		{
			l2_result->miss_hit = MISS_STORE;
		}
		if (not_full_block == true)
		{
			not_full(l2_cache_blocks, l1_l2_info->l2_associativity, l1_l2_info->l2_tag, loadstore);
		}
		else
		{
			miss_LRU(l2_cache_blocks, l2_result, l1_l2_info->l2_associativity, l1_l2_info->l2_tag, loadstore);
		}
	}
	return OK;
}



int l1_l2_entry_info_get(const struct cache_params l1_params,
						 const struct cache_params l2_params,
						 long address,
						 struct l1_l2_entry_info *l1_l2_info,
						 bool debug)
{
	int offset_l1 = log2(l1_params.block_size);
	int set_num_l1 = (l1_params.size*KB) / (l1_params.block_size*l1_params.asociativity);
	int idx_l1 = log2(set_num_l1);
	int tag_l1 = ADDRSIZE - idx_l1 - offset_l1;
	int offset_l2 = log2(l2_params.block_size);
	int set_num_l2 = (l2_params.size*KB) / (l2_params.block_size*l2_params.asociativity);
	int idx_l2 = log2(set_num_l2);
	int tag_l2 = ADDRSIZE - idx_l2 - offset_l2;
	int aux_l1 = address & (idx_l1 + offset_l1);
	l1_l2_info->l1_idx = aux_l1 >> offset_l1;
	l1_l2_info->l1_tag = address >> (idx_l1 + offset_l1);
	l1_l2_info->l1_associativity = l1_params.asociativity;
	int aux_l2 = address & (idx_l2 + offset_l2);
	l1_l2_info->l2_idx = aux_l2 >> offset_l2;
	l1_l2_info->l1_tag = address >> (idx_l2 + offset_l2);
	l1_l2_info->l2_associativity = l2_params.asociativity;
}
